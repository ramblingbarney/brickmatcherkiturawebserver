import Foundation
import CouchDB
import LoggerAPI

extension Brick {
  // 1
  class Persistence {
    // 2
    static func getAll(from database: Database, callback: 
      @escaping (_ bricks: [Brick]?, _ error: Error?) -> Void) {

//1
database.retrieveAll(includeDocuments: true) { documents, error in
  guard let documents = documents else {
    Log.error("Error retrieving all documents: \(String(describing: error))")
    return callback(nil, error)
  }
  //2
  let bricks = documents.decodeDocuments(ofType: Brick.self)
  callback(bricks, nil)
}
   
    }
    
    // 3
    static func save(_ brick: Brick, to database: Database, callback: 
      @escaping (_ brick: Brick?, _ error: Error?) -> Void) {

// 1
database.create(brick) { document, error in
  guard let document = document else {
    Log.error("Error creating new document: \(String(describing: error))")
    return callback(nil, error)
  }
  // 2
  database.retrieve(document.id, callback: callback)
}   
    }
    
    // 4
    static func delete(_ brickID: String, from database: Database, callback: 
      @escaping (_ error: Error?) -> Void) {

// 1
database.retrieve(brickID) { (brick: Brick?, error: CouchDBError?) in
  guard let brick = brick, let brickRev = brick._rev else {
    Log.error("Error retrieving document: \(String(describing:error))")
    return callback(error)
  }
  // 2
  database.delete(brickID, rev: brickRev, callback: callback)
}


    
    
    }
  }
}
