import XCTest

import BrickMatcherTests

var tests = [XCTestCaseEntry]()
tests += BrickMatcherTests.allTests()
XCTMain(tests)
