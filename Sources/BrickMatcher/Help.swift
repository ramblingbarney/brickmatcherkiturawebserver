// 1
import CouchDB

// 2
struct Help: Document {
  // 3
  let _id: String?
  // 4
  var _rev: String?
  // 5
  var headline: String
  var description: String
}
