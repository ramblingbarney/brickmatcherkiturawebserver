import Foundation
import CouchDB
import LoggerAPI

extension Help {
  // 1
  class Persistence {
    // 2
    static func getAll(from database: Database, callback: 
      @escaping (_ helps: [Help]?, _ error: Error?) -> Void) {

//1
database.retrieveAll(includeDocuments: true) { documents, error in
  guard let documents = documents else {
    Log.error("Error retrieving all documents: \(String(describing: error))")
    return callback(nil, error)
  }
  //2
  let helps = documents.decodeDocuments(ofType: Help.self)
  callback(helps, nil)
}
   
    }
    
    // 3
    static func save(_ help: Help, to database: Database, callback: 
      @escaping (_ help: Help?, _ error: Error?) -> Void) {

// 1
database.create(help) { document, error in
  guard let document = document else {
    Log.error("Error creating new document: \(String(describing: error))")
    return callback(nil, error)
  }
  // 2
  database.retrieve(document.id, callback: callback)
}   
    }
    
    // 4
    static func delete(_ helpID: String, from database: Database, callback: 
      @escaping (_ error: Error?) -> Void) {

// 1
database.retrieve(helpID) { (help: Help?, error: CouchDBError?) in
  guard let help = help, let helpRev = help._rev else {
    Log.error("Error retrieving document: \(String(describing:error))")
    return callback(error)
  }
  // 2
  database.delete(helpID, rev: helpRev, callback: callback)
}


    
    
    }
  }
}
