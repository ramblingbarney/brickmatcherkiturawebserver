import CouchDB
import Kitura
import KituraContracts
import LoggerAPI

// 1
private var database: Database?

func initializeBrickRoutes(app: App) {
  // 2
  database = app.database
  // 3
  app.router.get("/bricks", handler: getBricks)
  app.router.post("/bricks", handler: addBrick)
  app.router.delete("/bricks", handler: deleteBrick)
  app.router.get("/helps", handler: getHelp)
  app.router.post("/helps", handler: addHelp)
  app.router.delete("/helps", handler: deleteHelp)
}

// 4
private func getBricks(completion: @escaping ([Brick]?, 
  RequestError?) -> Void) {

guard let database = database else {
  return completion(nil, .internalServerError)
}
Brick.Persistence.getAll(from: database) { bricks, error in
  return completion(bricks, error as? RequestError)
}

  
}

// 5
private func addBrick(brick: Brick, completion: @escaping (Brick?, 
  RequestError?) -> Void) {

guard let database = database else {
  return completion(nil, .internalServerError)
}
Brick.Persistence.save(brick, to: database) { newBrick, error in
  return completion(newBrick, error as? RequestError)
}

}

// 6
private func deleteBrick(id: String, completion: @escaping 
  (RequestError?) -> Void) {

guard let database = database else {
  return completion(.internalServerError)
}
Brick.Persistence.delete(id, from: database) { error in
  return completion(error as? RequestError)
}

}

// help

// 7
private func getHelp(completion: @escaping ([Help]?,
  RequestError?) -> Void) {

guard let database = database else {
  return completion(nil, .internalServerError)
}
Help.Persistence.getAll(from: database) { helps, error in
  return completion(helps, error as? RequestError)
}


}

// 8
private func addHelp(help: Help, completion: @escaping (Help?,
  RequestError?) -> Void) {

guard let database = database else {
  return completion(nil, .internalServerError)
}
Help.Persistence.save(help, to: database) { newHelp, error in
  return completion(newHelp, error as? RequestError)
}

}

// 9
private func deleteHelp(id: String, completion: @escaping
  (RequestError?) -> Void) {

guard let database = database else {
  return completion(.internalServerError)
}
Help.Persistence.delete(id, from: database) { error in
  return completion(error as? RequestError)
}

}

