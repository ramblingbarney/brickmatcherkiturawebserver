// 1
import CouchDB

// 2
struct Brick: Document {
  // 3
  let _id: String?
  // 4
  var _rev: String?
  // 5
  var imageUrl: String
  var hexColor: String
  var matchedBrickID: String?
  var lat: Double?
  var lng: Double?
}
